# Selleks, et neid k�ske jooksutada, tuleb minna ../src/Hooldus.Web folderisse #

# Lisame uue migratiooni, kuid ei pane seda baasi #
dotnet ef --project ../MyGarage.Data --startup-project . migrations add "Migratiooni nimi"

# Eemaldame viimase migratsiooni, mida ei ole veel baasi viidud # 
dotnet ef --project ../MyGarage.Data --startup-project . migrations remove

# Viime viimase lisatud migratsiooni baasi #
dotnet ef --project ../MyGarage.Data --startup-project . database update

