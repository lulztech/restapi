﻿using System;
using Microsoft.EntityFrameworkCore;
using MyGarage.Data;

namespace MyGarage.IntegrationTest
{
    public abstract class BaseIntegrationTests : IDisposable
    {
        protected GarageContext Context { get; }

        protected BaseIntegrationTests()
        {
            const string conString = "Server=127.0.0.1;Port=5432;Database=dev;User Id=dev; Password=parool;";

            var dbOptionsBuilder = new DbContextOptionsBuilder();
            var options = dbOptionsBuilder.UseNpgsql(conString).Options;

            Context = new GarageContext(options);
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}