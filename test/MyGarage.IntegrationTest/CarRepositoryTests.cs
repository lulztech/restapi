﻿using System.Linq;
using MyGarage.Data.Repositories;
using MyGarage.Domain.Models;
using MyGarage.Domain.Services.Repositories;
using Xunit;

namespace MyGarage.IntegrationTest
{
    public class CarRepositoryTests : BaseIntegrationTests
    {
        private readonly ICarRepository carRepository;

        public CarRepositoryTests()
        {
            carRepository = new CarRepository(Context);
        } 

        [Fact]
        public void Insert_CarDoesntHaveId_AssignsId()
        {
            var car = new Car();

            carRepository.Insert(car);
            carRepository.Save();

            Assert.NotNull(car);
            Assert.NotEqual(car.CarId, 0);
        }

        [Fact]
        public void GetAll_ReturnsAtleastOne()
        {
            var cars = carRepository.GetAll().ToList();

            Assert.True(cars.Count > 0);
        }
    }
}