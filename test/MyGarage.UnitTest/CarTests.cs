﻿using System;
using MyGarage.Domain.Models;
using Xunit;

namespace MyGarage.UnitTest
{
    public class CarTests
    {
        [Fact]
        public void GetData_HasColorAndRegistrationNumber_ConcatenatesBoth()
        {
            var car = new Car
            {
                Color = "Punane",
                RegistrationNumber = "123"
            };

            Assert.Equal(car.GetData, "123:Punane");
        }

        [Fact]
        public void ValidateRegistrationNumber_WhichIsTooLong_ThrowsException()
        {
            var car = new Car
            {
                RegistrationNumber = "412888"
            };

            var ex = Assert.Throws<Exception>(() => car.ValidateRegistrationNumber());

            Assert.NotNull(ex);
            Assert.StartsWith("Registration number must contain", ex.Message);
        }
    }
}