﻿using System;
using System.Text.RegularExpressions;

namespace MyGarage.Domain.Models
{
    public class Car
    {
        public int CarId { get; set; }
        public string RegistrationNumber { get; set; }
        public string Color { get; set; }

        public string GetData => RegistrationNumber + ":" + Color;

        /* Example of domain logic */
        public void ValidateRegistrationNumber()
        {
            if (string.IsNullOrEmpty(RegistrationNumber))
                throw new Exception("Registration number is missing");

            if (!Regex.IsMatch(RegistrationNumber, @"\d{3}[a-z]{3}", RegexOptions.IgnoreCase))
                throw new Exception("Registration number must contain three consecutive digits and letters [123ABC].");
        }
    }
}