﻿using Microsoft.EntityFrameworkCore;
using MyGarage.Domain.Models;

namespace MyGarage.Data
{
    public class GarageContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }

        public GarageContext(DbContextOptions options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Car>();

            base.OnModelCreating(modelBuilder);
        }
    }
}