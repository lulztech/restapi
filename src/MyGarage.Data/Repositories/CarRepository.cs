﻿using System.Collections.Generic;
using System.Linq;
using MyGarage.Domain.Models;
using MyGarage.Domain.Services.Repositories;

namespace MyGarage.Data.Repositories
{
    public class CarRepository : BaseRepository<Car>, ICarRepository
    {
        public CarRepository(GarageContext context)
            : base(context)
        {}

        public override Car FindById(int id)
        {
            return DbSet.First(x => x.CarId == id);
        }

        public IEnumerable<Car> GetAll()
        {
            return DbSet.AsEnumerable();
        }
    }
}