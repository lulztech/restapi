﻿using Microsoft.EntityFrameworkCore;
using MyGarage.Domain.Services.Repositories;

namespace MyGarage.Data.Repositories
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private readonly GarageContext context;
        protected readonly DbSet<T> DbSet;

        protected BaseRepository(GarageContext context)
        {
            this.context = context;
            this.DbSet = context.Set<T>();
        }

        public void Insert(T entity)
        {
            DbSet.Add(entity);
        }

        public void Remove(T entity)
        {
            context.Remove(entity);
        }

        public abstract T FindById(int id);

        public void Save()
        {
            context.SaveChanges();
        }

        public void Update(T entity)
        {
            context.Update(entity);
        }
    }
}