﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using MyGarage.Data;

namespace MyGarage.Data.Migrations
{
    [DbContext(typeof(GarageContext))]
    [Migration("20161020194029_Init-Car")]
    partial class InitCar
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1");

            modelBuilder.Entity("MyGarage.Domain.Models.Car", b =>
                {
                    b.Property<int>("CarId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Color");

                    b.Property<string>("RegistrationNumber");

                    b.HasKey("CarId");

                    b.ToTable("Cars");
                });
        }
    }
}
