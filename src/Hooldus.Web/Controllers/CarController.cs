﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MyGarage.Domain.Models;
using MyGarage.Domain.Services.Services;

namespace Hooldus.Web.Controllers
{
    [Route("api/[controller]")]
    public class CarController : Controller
    {
        private readonly ICarService carService;

        public CarController(ICarService carService)
        {
            this.carService = carService;
        }

        // GET api/car
        [HttpGet]
        public IEnumerable<Car> Get()
        {
            var cars = carService.GetAllCars();

            return cars;
        }

        // GET api/car/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            throw new NotImplementedException();
        }

        // POST api/car
        [HttpPost]
        public void Post([FromBody]Car newCar)
        {
            carService.InsertNewCar(newCar);
        }

        // PUT api/car/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Car value)
        {
            throw new NotImplementedException();
        }

        // DELETE api/car/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}