﻿using System.Collections.Generic;
using MyGarage.Domain.Models;

namespace MyGarage.Domain.Services.Repositories
{
    public interface ICarRepository : IBaseRepository<Car>
    {
        IEnumerable<Car> GetAll();
    }
}