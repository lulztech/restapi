﻿namespace MyGarage.Domain.Services.Repositories
{
    public interface IBaseRepository<T> where T : class
    {
        void Insert(T entity);
        void Update(T entity);
        void Remove(T entity);
        T FindById(int id);

        void Save();
    }
}