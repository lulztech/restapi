﻿using System.Collections.Generic;
using MyGarage.Domain.Models;
using MyGarage.Domain.Services.Repositories;

namespace MyGarage.Domain.Services.Services
{
    public interface ICarService
    {
        void InsertNewCar(Car car);
        IEnumerable<Car> GetAllCars();
    }

    public class CarService : ICarService
    {
        private readonly ICarRepository carRepository;

        public CarService(ICarRepository carRepository)
        {
            this.carRepository = carRepository;
        }

        public void InsertNewCar(Car car)
        {
            car.ValidateRegistrationNumber();

            carRepository.Insert(car);
            carRepository.Save();
        }

        public IEnumerable<Car> GetAllCars()
        {
            return carRepository.GetAll();
        }
    }
}